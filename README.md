#logback-web

一般情况下，如果在同一个web容器中有多个项目用到logback（下面都以tomcat为例，其他容器类似）

1）每个项目都自带logback的lib包和配置文件（logback.xml）

    缺点：项目多的话一堆重复的第三方jar包，打war包发布更新的时候慢

2）把lib包和配置文件都放在tomcat/lib下

    缺点：多个项目的配置写在同一个文件中，互相干扰，发布完还得手动改，不方便

为了解决该问题，建立了logback-web项目

内容非常简单，就一个类+一个配置文件

达到的效果：将logback的jar包和logback-web的包都放在tomcat/lib下，而每个项目自己的日志配置文件（logback.xml或logback.groovy）放在自己的WEB-INF/classes下，logback自动为每个项目分别初始化LoggerFactory

原理：logback读取配置文件的顺序为

1.classpath下（如果jar包放在tomcat/lib下它就会到这个目录找）

2.寻找Configurator的第一个实现类

3.代码内默认的配置（以固定格式输出日志到console）

logback-web就是实现了Configurator，根据每个项目初始化线程找到项目路径，分别读取配置文件